# Custom Stream Overlay for CRG

C'est le code de l'overlay de streaming personnalisé que j'ai fait pour le SAM !
C'est pas très commenté, mais j'explique mon processus ici : [Article de blog sur la création de l'overlay](https://blog.fune.li/creer-un-overlay-carolina-personnalise.html).

## Update du 17 Juillet 2024

- Ajout de commentaires dans index.js pour indiquer quelle fonction sert à quoi
- Prise en compte du "Lost" qui retire maintenant l'étoile du lead sur l'affichage lorsqu'il est coché.

**Je crois que l'overlay permet maintenant d'afficher toutes les informations (minimales) nécessaires pour pouvoir streamer un match !**
Les fonctionnalités/informations en question : 
- Nom de chaque équipe
- Nom du player ayant le statut de jammer
- Score de chaque équipe
- Points marqués par chaque équipe au jam en cours
- Nombre de TTO et d'OR disponible par équipe
- Affiche une étoile à côté du derby name de lae lead jammer
- Retire l'étoile quand le lead est lost
- Prend en compte les star pass dans l'affichage du derby name de lae jammer
- Affiche le temps de Jam -ou Lineup, ou Timeout- et le temps de Période
- Affiche le type de timeout lorsqu'un timeout est pris (et indique quelle équipe le prend le cas échéant)

Les fonctionnalités d'affichage des rosters, des pénalités, des points marqués par jam ne sont **pas** intégrées dans cet overlay.

La font Fugaz One a été designée par LatinoType sous licence OFL (Open Font Licence)

Mon code (le reste) est en copyleft parce que balek.
