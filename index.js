$(initialize); // This line runs the "initialize" function when this file is run.

        function initialize() {
            WS.Connect();
            WS.AutoRegister(); // These two lines must be present for this script to communicate with the scoreboard.
            
            //Gathers all information to display on the overlay template from the functions below
            WS.Register(
                "ScoreBoard.CurrentGame",
                function gameInfos(k,v) {
                    updateJamClock(k,v);
                    updatePeriodClock(k,v);
                    l(k,v);
                    r(k,v);
                    colors(k,v);
                    lead(k,v);
                    scores(k,v);
                    timeouts(k,v);
                }
            )
            
            function updateJamClock(k,v){
                var timeout = WS.state["ScoreBoard.CurrentGame.TimeoutOwner"];
                var ot = WS.state["ScoreBoard.CurrentGame.OfficialReview"];
                var lId = WS.state["ScoreBoard.CurrentGame.Team(1).Id"];
                var rId = WS.state["ScoreBoard.CurrentGame.Team(2).Id"];
                var type = k.Clock;
                var running = k.Running;
                if (type == "Jam") {
                    var time = WS.state["ScoreBoard.CurrentGame.Clock(Jam).Time"];
                    $("#jamclock").html(_timeConversions.msToMinSec(time));
                    $("#timeout-type").html(" ");
                    $("#timeout-type").removeClass("t-right");
                    $("#timeout-type").removeClass("t-left");
                    $("#jammername-l").addClass("visible");
                    $("#jammername-r").addClass("visible");
                } else if (type == "Timeout") {
                    var time = WS.state["ScoreBoard.CurrentGame.Clock(Timeout).Time"];
                    $("#jamclock").html(_timeConversions.msToMinSec(time));
                    $("#timeout-type").show();
                    $("#timeout-type").html("Timeout");
                    $("#jammername-l").removeClass("visible");
                    $("#jammername-r").removeClass("visible");
                    if (timeout == lId) {
                        if (ot === true) {
                            $("#timeout-type").html("Official Review");
                            $("#timeout-type").addClass("t-left");
                            $("#timeout-type").removeClass("t-right");
                        } else {
                            $("#timeout-type").html("Team Timeout");
                            $("#timeout-type").addClass("t-left");
                            $("#timeout-type").removeClass("t-right");
                        }
                    } else if (timeout == rId) {
                        if (ot === true) {
                            $("#timeout-type").html("Official Review");
                            $("#timeout-type").addClass("t-right");
                            $("#timeout-type").removeClass("t-left");
                        } else {
                            $("#timeout-type").html("Team Timeout");
                            $("#timeout-type").addClass("t-right");
                            $("#timeout-type").removeClass("t-left");
                        }
                    } else if (timeout == "O") {
                        $("#timeout-type").html("Official Timeout");
                        $("#timeout-type").removeClass("t-right");
                        $("#timeout-type").removeClass("t-left");
                    }
                } else if (type == "Lineup") {
                    var time = WS.state["ScoreBoard.CurrentGame.Clock(Lineup).Time"];
                    $("#jamclock").html(_timeConversions.msToMinSec(time));
                    $("#timeout-type").html(" ");
                    $("#timeout-type").removeClass("t-right");
                    $("#timeout-type").removeClass("t-left");
                    $("#jammername-l").removeClass("visible");
                    $("#jammername-r").removeClass("visible");
                }
            }
            function updatePeriodClock(k,v){
                var type = k.Clock;
                if (type == "Period") {
                    var time = WS.state["ScoreBoard.CurrentGame.Clock(Period).Time"];
                    $("#clock").html(_timeConversions.msToMinSec(time));
                } else if (type == "Intermission") {
                    var time = WS.state["ScoreBoard.CurrentGame.Clock(Intermission).Time"];
                    $("#clock").html(_timeConversions.msToMinSec(time));
                    $("#jamclock").html(" ");
                }
            }
            // getting Information for higher seed or home team (on the left left of the screen)
            function l(k,v) {
                var period = WS.state["ScoreBoard.CurrentGame.CurrentPeriodNumber"];
                var jam = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").CurrentJamNumber"];
                var name = WS.state["ScoreBoard.CurrentGame.Team(1).TeamName"];
                var jammerId = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").Jam(" + jam + ").TeamJam(1).Fielding(Jammer).Skater"];
                var jammer = WS.state["ScoreBoard.CurrentGame.Team(1).Skater(" + jammerId + ").Name"];
                var starpass = WS.state["ScoreBoard.CurrentGame.Team(1).StarPass"];
                var pivotId = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").Jam(" + jam + ").TeamJam(1).Fielding(Pivot).Skater"];
                var pivot = WS.state["ScoreBoard.CurrentGame.Team(1).Skater(" + pivotId + ").Name"];
                var jammerName = jammer;

                $("#team-left").html(name);
                
                if (starpass === true) {
                    jammerName = pivot;
                } 
                if (starpass === false) {
                    jammerName = jammer
                }
                if (jammerName === undefined) {
                    $("#jammername-l").html(" ");
                }
                $("#jammername-l").html(jammerName);
            }
            // getting Information for visitor team or lower seed displayed on the right of the screen
            function r(k,v) {
                var period = WS.state["ScoreBoard.CurrentGame.CurrentPeriodNumber"];
                var jam = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").CurrentJamNumber"];
                var name = WS.state["ScoreBoard.CurrentGame.Team(2).TeamName"];
                var jammerId = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").Jam(" + jam + ").TeamJam(2).Fielding(Jammer).Skater"];
                var jammer = WS.state["ScoreBoard.CurrentGame.Team(2).Skater(" + jammerId + ").Name"];
                var starpass = WS.state["ScoreBoard.CurrentGame.Team(2).StarPass"];
                var pivotId = WS.state["ScoreBoard.CurrentGame.Period(" + period + ").Jam(" + jam + ").TeamJam(2).Fielding(Pivot).Skater"];
                var pivot = WS.state["ScoreBoard.CurrentGame.Team(2).Skater(" + pivotId + ").Name"];
                var jammerName = jammer;
                
                $("#team-right").html(name);
                
                if (starpass === true) {
                    jammerName = pivot;
                } 
                if (starpass === false) {
                    jammerName = jammer;
                }
                if (jammerName === undefined) {
                    $("#jammername-r").html(" ");
                }
                $("#jammername-r").html(jammerName);
            }
            // Getting colors for each team
            function colors(k,v) {
                var root = document.querySelector(':root');
                var fgColorL = WS.state['ScoreBoard.CurrentGame.Team(1).Color(overlay_fg)'];
                var bgColorL = WS.state['ScoreBoard.CurrentGame.Team(1).Color(overlay_bg)'];
                root.style.setProperty('--fgleft', fgColorL);
                root.style.setProperty('--bgleft', bgColorL);
                var fgColorR = WS.state['ScoreBoard.CurrentGame.Team(2).Color(overlay_fg)'];
                var bgColorR = WS.state['ScoreBoard.CurrentGame.Team(2).Color(overlay_bg)'];
                root.style.setProperty('--fgright', fgColorR);
                root.style.setProperty('--bgright', bgColorR);
                console.log(root);
            }
            // Getting information on lead status (Lead and/or lost)
            function lead(k,v) {
                var rLead = WS.state["ScoreBoard.CurrentGame.Team(2).Lead"];
                var lLead = WS.state["ScoreBoard.CurrentGame.Team(1).Lead"];
                var rLost = WS.state["ScoreBoard.CurrentGame.Team(2).Lost"];
                var lLost = WS.state["ScoreBoard.CurrentGame.Team(1).Lost"];
                if (rLead === true) {
                    $("#jammername-r").addClass("lead");
                    $("#jammername-l").removeClass("lead");
                }
                if (lLead === true) {
                    $("#jammername-l").addClass("lead");
                    $("#jammername-r").removeClass("lead");
                }
                if (rLead === false) {
                    $("#jammername-r").removeClass("lead");
                }
                if (lLead === false) {
                    $("#jammername-l").removeClass("lead");
                }
                if (rLost === true) {
                    $("#jammername-r").removeClass("lead");
                }
                if (lLost === true) {
                    $("#jammername-l").removeClass("lead");
                }
            }

            function scores(k,v) {
                var r = WS.state["ScoreBoard.CurrentGame.Team(2).Score"];
                var l = WS.state["ScoreBoard.CurrentGame.Team(1).Score"];
                var rj = WS.state["ScoreBoard.CurrentGame.Team(2).JamScore"];
                var lj = WS.state["ScoreBoard.CurrentGame.Team(1).JamScore"];
                $("#totalscore-l").html(l);
                $("#jamscore-l").html(lj);
                $("#totalscore-r").html(r);
                $("#jamscore-r").html(rj);
            }

            function timeouts(k,v) {
                var ttr = WS.state["ScoreBoard.CurrentGame.Team(2).Timeouts"];
                var ttl = WS.state["ScoreBoard.CurrentGame.Team(1).Timeouts"];
                var orr = WS.state["ScoreBoard.CurrentGame.Team(2).OfficialReviews"];
                var orl = WS.state["ScoreBoard.CurrentGame.Team(1).OfficialReviews"];
                $("#tt-l").html("T ".repeat(ttl));
                $("#or-l").html("O ".repeat(orl));
                $("#tt-r").html("T ".repeat(ttr));
                $("#or-r").html("O ".repeat(orr));
            }
        }